<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

class User extends Entity{

    protected function _setPassword($password){

        $password_hashed = (new DefaultPasswordHasher())->hash($password);
        return $password_hashed;

    }

}

