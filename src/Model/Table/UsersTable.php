<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

    }

    public function validationDefault(\Cake\Validation\Validator $validator)
    {

        parent::validationDefault($validator);

        $validator->requirePresence('pseudo', 'create');
        $validator->notEmptyString('pseudo');
        $validator->requirePresence('adresse_mail', 'create');
        $validator->notEmptyString('adresse_mail');
        $validator->requirePresence('password', 'create');
        $validator->notEmptyString('password');

        $validator -> add('role', 'inList', [
            'rule' => ['inList', ['admin', 'auteur']],
            'message' => 'Rôle non autorisé'
        ]);

        return $validator;
    }
}
