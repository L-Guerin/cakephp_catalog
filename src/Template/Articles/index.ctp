<h1>Liste des articles</h1>

<?php if($user_role == 'admin'){ ?>
<table>
    <tr>
        <th>Title</th>
        <th>Contenu</th>
        <th></th>
        <th></th>
    </tr>
    <?php
    foreach ($articles as $article){
        ?>
        <tr>
        <td><?= $article->title ?></td>
        <td><?= $article->content ?></td>
        <td><?= $this->Html->link('Modifier', ['action' => 'edit', $article->id]);?></td>
        <td><?= $this->Form->postLink('Supprimer', ['action' => 'delete', $article->id] ,['confirm' => 'Etes-vous sur?']); ?></td>
        <?php } ?>
        </tr>
</table>
<?php } else { ?>
<table>
    <tr>
        <th>Title</th>
        <th>Contenu</th>

    </tr>
    <?php
    foreach ($articles as $article){
    ?>
    <tr>
        <td><?= $article->title ?></td>
        <td><?= $article->content ?></td>
        <?php } ?>
    </tr>
</table>
<?php } ?>
<?php if($user_role == 'admin'){ ?>
    <a class="button" href="<?= $this->Url->build('/articles/add') ?>">Ajouter une page</a>
<?php } ?>
