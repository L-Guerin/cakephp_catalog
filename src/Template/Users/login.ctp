<div class="users form">
    <?php echo $this->Form->create();?>
    <fieldset>
        <legend><?='Connexion'; ?></legend>
        <?php
        echo $this->Form->control('pseudo');
        echo $this->Form->control('password');
        echo $this->Form->button('Connexion');
        echo $this->Form->end();
        ?>
    </fieldset>
</div>

<a class="button" href="<?= $this->Url->build('/users/add')?>">S'inscrire</a>
