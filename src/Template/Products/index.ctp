<h1>Liste des produits</h1>
<center>
    Trier par catégorie :<br/>

      <?php foreach ($products as $product){ ?>
              <a href="<?= $this->Url->build('/products/sort/'.$product->category_name); ?>"><?=$product->category_name?></a>
      <?php } ?>
</center>
<br/><br/>
<?php if($user_role == 'admin'){ ?>
    <table>
        <tr>
          <th>Titre</th>
          <th>Description</th>
          <th>Categorie</th>
          <th>Prix</th>
          <th></th>
          <th></th>
        </tr>

    <?php foreach ($products as $product){ ?>

        <tr>
          <td><h3><a href="<?= $this->Url->build('/products/view/'.$product->name) ?>"><?= $product->name ?></a></h3></td>
          <td><?= $product->description ?></td>
          <td><?= $product->category_name ?></td>
          <td><?= $product->price ?> €</td>

          <td><?= $this->Html->link('Modifier', ['action' => 'edit', $product->id]);?></td>
          <td><?= $this->Form->postLink('Supprimer', ['action' => 'delete', $product->id] ,['confirm' => 'Etes-vous sur?']); ?></td>


        </tr>

    <?php } ?>

    </table>
<?php } else{ ?>

    <table>
        <tr>
            <th>Titre</th>
            <th>Description</th>
            <th>Categorie</th>
            <th>Prix</th>

        </tr>

        <?php foreach ($products as $product){ ?>

            <tr>
                <td><h3><a href="<?= $this->Url->build('/products/view/'.$product->name) ?>"><?= $product->name ?></a></h3></td>
                <td><?= $product->description ?></td>
                <td><?= $product->category_name ?></td>
                <td><?= $product->price ?> €</td>

            </tr>

        <?php } ?>

    </table>

<?php } ?>

<?php if($user_role == 'admin'){ ?>
    <a class="button" href="<?= $this->Url->build('/products/add') ?>">Ajouter un produit</a>
<?php } ?>
