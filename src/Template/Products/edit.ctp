<?php
echo $this->Form->create($product);
echo $this->Form->control('name');
echo $this->Form->control('description');
echo $this->Form->control('price');
echo $this->Form->control('category_name',['options'=>['sécurité'=>'sécurité','sport'=>'sport','maison'=>'maison','voyage'=>'voyage']]);
echo $this->Form->button(__('Sauvegarder l\'article'));
echo $this->Form->end();
?>
