<table>
    <tr>
        <th>Titre</th>
        <th>Description</th>
        <th>Categorie</th>
        <th>Prix</th>
    </tr>

    <tr>
        <td><h3><a href="<?= $this->Url->build('/products/view/'.$product->name) ?>"><?= $product->name ?></a></h3></td>
        <td><?= $product->description ?></td>
        <td><?= $product->category_name ?></td>
        <td><?= $product->price ?> €</td>

    </tr>

</table>
