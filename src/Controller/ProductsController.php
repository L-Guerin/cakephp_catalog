<?php
namespace App\Controller;

use App\Controller\AppController;

class ProductsController extends AppController
{
  public function index()
  {
      $categories=$this->Products->Categories->find('all');
      $this->set('categories',$categories);

      $products=$this->Products->find('all');
      $this->set('products',$products);
      $this->set('user_role', $this->Auth->user('role'));
  }


  public function sort($name)
  {
      $product=$this->Products->find('all')
          ->where(['Products.category_name' => $name])
          ->first();
      $this->set('product',$product);
  }

  public function view($product_url)
  {
    $product = $this->Products
              ->find()
              ->where(['products.name' => $product_url])
              ->first();
    $this->set('product', $product);
  }

  public function add()
  {
      if($this->Auth->user('role') != 'admin'){

          return $this->redirect(['action' => 'index']);
      }
      $products = $this->Products->newEntity();
      if ($this->request->is('post'))
      {
          $products = $this->Products->patchEntity($products, $this->request->getData());
          if ($this->Products->save($products))
          {
              $this->Flash->success(__('Votre produit a été sauvegardé.'));
              return $this->redirect(['action' => 'index']);
          }
          $this->Flash->error(__("Impossible d'ajouter votre produit."));
      }
      $this->set('products', $products);
  }


  public function edit($product_id)
  {
      if($this->Auth->user('role') != 'admin'){

          return $this->redirect(['action' => 'index']);
      }
      $product = $this->Products->find()->where(['products.id' => $product_id])->first();
      if ($this->request->is('put'))
      {
          $product = $this->Products->patchEntity($product, $this->request->getData());

          if ($this->Products->save($product))
          {
              $this->Flash->success(__('Votre produit a été sauvegardé.'));
              return $this->redirect(['action' => 'index']);
          }
      }

      $this->set('product', $product);
  }


  public function delete($product_id)
  {
      if($this->Auth->user('role') != 'admin'){

          return $this->redirect(['action' => 'index']);
      }
      $this->render(false, false);
      $delproduct = $this->Products->find()->where(['products.id' => $product_id])->first();

      if (!is_null($delproduct) && $this->Products->delete($delproduct))
      {

          $this->Flash->success(__('Votre produit a été supprimé.'));
          return $this->redirect(['action' => 'index']);
      }
      else
      {
          $this->Flash->error('Votre produit n \'a pas pu être supprimé');
          return $this->redirect(['action' => 'index']);
      }
  }
}

?>
