<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         3.3.4
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Event\Event;

/**
 * Error Handling Controller
 *
 * Controller used by ExceptionRenderer to render error responses.
 */
class ArticlesController extends AppController
{

    public function index()
    {
        $articles = $this->Articles->find('all');
        $this->set('articles', $articles);
        $this->set('user_role', $this->Auth->user('role'));
    }

    public function add()
    {
        if($this->Auth->user('role') != 'admin'){

            return $this->redirect(['action' => 'index']);
        }
        $articles = $this->Articles->newEntity();
        if ($this->request->is('post'))
        {
            $articles = $this->Articles->patchEntity($articles, $this->request->getData());
            $articles->user_id = $this->Auth->user('id');
            if ($this->Articles->save($articles))
            {
                $this->Flash->success(__('Votre article a été sauvegardé.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__("Impossible d'ajouter votre article."));
        }
        $this->set('articles', $articles);
    }

    public function edit($article_id)
    {
        if($this->Auth->user('role') != 'admin'){

            return $this->redirect(['action' => 'index']);
        }
        $article = $this->Articles->find()->where(['Articles.id' => $article_id])->first();
        if ($this->request->is('put')) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Votre article a été sauvegardé.'));
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('article', $article);
    }

    public function delete($article_id)
    {
        if($this->Auth->user('role') != 'admin'){

            return $this->redirect(['action' => 'index']);
        }
        $this->render(false, false);
        $delArticle = $this->Articles->find()->where(['Articles.id' => $article_id])->first();

        if (!is_null($delArticle) && $this->Articles->delete($delArticle))
        {

            $this->Flash->success(__('Votre page a été supprimé.'));
            return $this->redirect(['action' => 'index']);
        }else{
            $this->Flash->error('WOW 9A MARCHE PAS');
            return $this->redirect(['action' => 'index']);
        }

    }
}
