<?php
//PULL
    namespace App\Controller;

    use App\Controller\AppController;
    use Cake\Core\Configure;
    use Cake\Http\Exception\ForbiddenException;
    use Cake\Http\Exception\NotFoundException;
    use Cake\View\Exception\MissingTemplateException;

    class UsersController extends AppController
    {
        public function beforeFilter(\Cake\Event\Event $event) {
            parent::beforeFilter($event);
            $this->Auth->allow(['add']);
        }

        public function index()
        {

        }

        public function login() {
            if ($this->getRequest()->is('post'))
            {
                $user = $this->Auth->identify();

                if($user){
                    $this->Auth->setUser($user);

                    return $this->redirect('/');
                }
                $this->Flash->error('CA MARCHE PAS');
            }
        }

        public function logout() {
            $this->render(false, false);
            if(true)
            {
                $this->Flash->success('Vous avez été déconnecté');
                return $this->redirect($this->Auth->logout());
            }
            else
            {
                $this->Flash->error('Vous n\'êtes pas connecté');
            }
        }

        public function add() {
            $addUsers = $this->Users->newEntity();
            if ($this->request->is('post'))
            {
                $addUsers = $this->Users->patchEntity($addUsers, $this->request->getData());
                if ($this->Users->save($addUsers))
                {
                    $this->Flash->success(__('Votre utilisateur a été sauvegardé.'));
                    return $this->redirect(['action' => 'index']);
                }

                $this->Flash->error(__("Impossible d'ajouter votre article."));
            }
            $this->set('addUsers', $addUsers);
        }
    }
