<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CategoriesController extends AppController
{
  public function index()
  {

  }
  
  public function add()
  {
      $categories = $this->Categories->newEntity();
      if ($this->request->is('post'))
      {
          $categories = $this->Categories->patchEntity($categories, $this->request->getData());
          if ($this->Categories->save($categories))
          {
              $this->Flash->success(__('Votre produit a été sauvegardé.'));
              return $this->redirect(['action' => 'index']);
          }
          $this->Flash->error(__("Impossible d'ajouter votre produit."));
      }
      $this->set('categories', $categories);
  }
}
